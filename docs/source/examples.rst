.. _examples:


Examples
===========================

Here are a few jupyter notebooks to demonstrate the code.

To run a notebook on your machine :
   1. If you don't have the source code locally, use git to clone the repo.
      (See :ref:`developer install instructions <installation>` for cloning.)
   2. Make sure you have jupyter and matplotlib installed (instructions 
      :ref:`here <installation>`.)
   3. In an Anaconda prompt, change to the directory with the notebooks and  
      enter ``jupyter notebook``.  
   4. Your default internet browser should open and show your current directory  
   5. Click on the notebook you want to run and it will open in a new tab

The notebooks are located in ``docs/source/notebooks`` in the repository.

.. toctree::
    :caption: Examples
    :maxdepth: 2

    notebooks/demo_met_mast