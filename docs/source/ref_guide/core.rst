.. _core:


Base classes
--------------

.. automodule:: vtmeas.core

.. autoclass:: vtmeas.TurbBox
    :members: read_hawc2, read_uvw, plot_slice, interpolate

