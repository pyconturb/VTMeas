.. _instruments:


Instruments
--------------

.. automodule:: vtmeas.instruments

.. autoclass:: vtmeas.instruments.CupAnemometer
    :members: measure

.. autoclass:: vtmeas.instruments.SonicAnemometer
    :members: measure

