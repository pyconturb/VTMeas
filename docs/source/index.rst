.. VTMeas documentation master file


Welcome to VTMeas
===========================================

*- Virtual measurements of turbulence boxes for generating constrained turbulence.*

A package for sampling a turbulence box as if a measurement device was there.
Outputs constraints that can be fed directly into
`PyConTurb <https://rink.pages.windenergy.dtu.dk/pyconturb/>`_ for constrained turbulence
generation.

The source code is located
`here on GitLab <https://gitlab.windenergy.dtu.dk/rink/VTMeas>`_.


Contents:
    .. toctree::
        :maxdepth: 2
    
        installation
        reference_guide
        examples