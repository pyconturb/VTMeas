{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Virtual Met Mast\n",
    "\n",
    "Let's examine the creation of a virtual met mast.\n",
    "\n",
    "Sections:  \n",
    "1. [Preliminaries](#Preliminaries)  \n",
    "2. [Define met mast](#Define-met-mast)  \n",
    "3. [Load/examine turbulence](#Load/examine-turbulence)  \n",
    "4. [Measure turbulence](#Measure-turbulence)  \n",
    "5. [Define custom instruments](#Define-custom-instruments)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preliminaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pkg_resources\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from vtmeas import MetMast, TurbBox\n",
    "from vtmeas.instruments import SonicAnemometer\n",
    "from vtmeas._turb._generate_bin import _REQ_KWARGS as turb_specs\n",
    "\n",
    "turb_path = pkg_resources.resource_filename('vtmeas', '_turb/demo_{c}.bin')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define met mast\n",
    "\n",
    "We first define our 5 sonic anemometers with 10-Hz sampling frequency and place them onto the met mast."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_samp = 10  # sample at 10 Hz\n",
    "z_sonics = np.linspace(29, 209, 5)\n",
    "sonic_anems = [SonicAnemometer([0, 0, z], f'{z}m SonAnem', f_samp) for z in z_sonics]\n",
    "mm = MetMast(z_sonics[-1], instruments=sonic_anems)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can make a diagram of the met mast to make sure it looks like what we expect."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mm.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load/examine turbulence\n",
    "\n",
    "We will use the demo turbulence packaged with VTMeas. It was generated using the [standalone Mann turbulence generator](http://www.hawc2.dk/download/pre-processing-tools). For details on the inputs, see `vtmeas/_turb/generate_bin.py`.\n",
    "\n",
    "We first load the turbulence boxes into the VTMeas `TurbBox` object. This requires a dictionary specifying the spatial specifications of the box, `turb_specs`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tb = TurbBox().read_hawc2(turb_path, **turb_specs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can examine `turb_specs` to see what's in it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "turb_specs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `TurbBox` comes with several useful methods and attributes. Here are some examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tb.u.shape  # access the nx-ny-nz turbulence box of u"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tb.t, tb.w[:, 0, 0]);  # plot the vertical wind at point 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tb.plot_slice(0, 'u');  # plot the u component at t=0 (grey pts are location)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Measure turbulence\n",
    "\n",
    "The met mast object has a method called `measure`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "con_df = mm.measure(tb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output is a pandas dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "con_df.head()  # look at the top of the dataframe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each instrument is indicated by `i#`, where `#` indicates its order in the instrument list. `k` is component (u, v or w). We can compare the u component at location [7, 0] and [8, 0] to the lowest sonic anemometer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time_idx = con_df.index.map(lambda x: isinstance(x, (int, float)))  # index with time\n",
    "time_con = con_df[time_idx]  # pull out time data\n",
    "plt.plot(tb.t, tb.u[:, 7, 0], label='u [7, 0]')  # turb point near Inst0\n",
    "plt.plot(tb.t, tb.u[:, 8, 0], label='u [8, 0]')  # turb point near Inst0\n",
    "plt.plot(time_con.index, time_con.i0_u_p0, label='Inst0')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen, the sonic anemometer measurement is an interpolation between the two points. This is a design choice within the package. The measurement procedure, in particular, is:  \n",
    "1. Interpolate the `TurbBox` object within the 2D plane to necessary y-z locations using bilinear interpolation  \n",
    "2. Interpolate the new 2D planes to required time locations using cubic interpolation in time  \n",
    "3. Pull out the u, v, w components at the required time/space  \n",
    "4. Perform any calculations necessary (e.g., cup anemometer is $\\sqrt{u^2+v^2}$)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define custom instruments\n",
    "\n",
    "Perhaps the instruments included in VTMeas are insufficient for your needs. In this case, you can easily make your own instrument. All you need to do is define a Python class with a method called `measure` that takes a turbulence box object as input and returns a correctly formatted dataframe as output. In particular, it must have rows `k`, `x`, `y` and `z` followed by the time steps.\n",
    "\n",
    "Here is a silly example of an instrument that returns 1 if any u in the y-z plane is greater than 20 and 0 else."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SillyInstrument:\n",
    "    \"\"\"A nonsensical instrument\"\"\"\n",
    "    def measure(self, turb_box):\n",
    "        index = pd.concat((pd.Series(['k', 'x', 'y', 'z']), pd.Series(turb_box.t)))\n",
    "        con_df = pd.DataFrame(index=index)\n",
    "        con_df.loc[['k', 'x', 'y', 'z'], 's'] = 0\n",
    "        con_df.loc[turb_box.t, 's'] = np.any(turb_box.u > 20, axis=(1, 2))\n",
    "        con_df['t'] = con_df.index.map(lambda x: isinstance(x, (int, float)))\n",
    "        return con_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mm_silly = MetMast(0, instruments=[SillyInstrument()])\n",
    "silly_con = mm_silly.measure(tb)\n",
    "time_idx = silly_con.index.map(lambda x: isinstance(x, (int, float)))  # index with time\n",
    "silly_time = silly_con[time_idx]\n",
    "silly_time.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(silly_time.index, silly_time.i0_s);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
