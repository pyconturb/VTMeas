# -*- coding: utf-8 -*-
"""Setup file for VTMeas library

See project documentation for how to use this file.
"""
from setuptools import setup

exec(open('vtmeas/_version.py').read())  # get version and release


setup(name='vtmeas',
      version=__version__,
      description='Virtual measurements of turbulence boxes',
      url='https://gitlab.windenergy.dtu.dk/rink/VTMeas',
      author='Jenni Rinker',
      author_email='rink@dtu.dk',
      license='MIT',
      packages=['vtmeas',  # top-level package
                ],
      package_data={'vtmeas': ['vtmeas/*.bin']},  # bundle demo turbs
      install_requires=['matplotlib',  # plotting methods in classes
                        'numpy',  # numeric arrays
                        'pandas',  # column-labelled arrays
                        'scipy',  # interpolating turbulence grid
                        ],
      zip_safe=False)
