# -*- coding: utf-8 -*-
"""Generate bin files necessary for examples
"""
from pathlib import Path
import subprocess

import numpy as np

from vtmeas._utils import bin_to_arr

_NX, _NY, _NZ = 256, 16, 16
_UADV, _ZMID, _WID, _T, _ALPHA = 10, 119, 180, 60, 0.2
_TURB_NAME = 'demo'
_REQ_KWARGS = {'ny': _NY, 'nz': _NZ, 'dx': (_UADV*_T)/_NX, 'dy': _WID/(_NY-1),
               'dz': _WID/(_NZ-1), 'z_mid': _ZMID, 'u_adv': _UADV}


def run_mann():
    """run standalone mann turbulence executable"""
    exe_path = Path('C:/Users/rink/Documents/hawc2/mann_turb_x64/mann_turb_x64.exe')
    turb_dir = Path('.').absolute()
    locals().update(_REQ_KWARGS)
    turb_specs = [1.0, 29.4, 3.0, 1337, _NX, ny, nz, dx, dy, dz, True]  # ae,l,gam,seed
    exe_in = [str(x) for x in turb_specs]
    out = subprocess.run([exe_path.as_posix(), f'{(turb_dir / _TURB_NAME)}'] + exe_in,
                         capture_output=True)
    if out.returncode:
        raise ValueError('Mann turbulence generation failed! Exe output:\n' +
                         f'{out.stdout.decode("utf-8")}\n{out.stderr.decode("utf-8")}')
    return out


def add_shear():
    """add the shear into the mann u.bin file"""
    turb_path = str(list(Path('.').glob('*u.bin'))[0])
    u_arr = bin_to_arr(turb_path, _NY, _NZ)
    z = np.linspace(-_WID/2, _WID/2, _NZ, endpoint=True) + _ZMID
    wsp = _UADV * (z/_ZMID) ** _ALPHA
    u_arr += wsp
    u_arr.tofile(turb_path)


if __name__ == '__main__':
    out = run_mann()  # create turbulence box
    add_shear()  # add shear
