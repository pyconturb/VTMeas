# -*- coding: utf-8 -*-
"""Test instruments
"""
import numpy as np
import pandas as pd

from vtmeas import TurbBox
from vtmeas.instruments import SonicAnemometer, CupAnemometer


_SIMP_UVW_DICT = {'uvw'[ic]: np.array([[[0, 0, 0], [1, 1, 1]], [[1, 1, 1], [2, 2, 2]]])
                  + ic for ic in range(3)}  # simple uvw turbulence boxes
_SIM_UVW_KWARGS = {'ny': 2, 'nz': 3, 'dx': 1, 'dy': 180, 'dz': 90, 'u_adv': 1,
                   'z_mid': 119}  # keyword arguments for simple uvw box


def test_sonic_measure():
    """Verify sonic returns correct measurement (incl. wrapping in x)"""
    # given
    loc, name = [0, 0, 119], 'test sa'
    f1, f2 = 2, 1/0.9  # (T-tstart)/dt (1) is and (2) is not an integer
    tb = TurbBox().read_uvw(**_SIMP_UVW_DICT, **_SIM_UVW_KWARGS)
    theo_df1 = pd.DataFrame([[0, 1, 2], [0, 0, 0], [0, 0, 0],
                             [119, 119, 119], [0.5, 1.5, 2.5],
                             [1, 2, 3], [1.5, 2.5, 3.5], [1, 2, 3]],
                            columns=['u_p0', 'v_p0', 'w_p0'],
                            index=['k', 'x', 'y', 'z', 0, 0.5, 1, 1.5])
    theo_df2 = pd.DataFrame([[0, 1, 2], [0, 0, 0], [0, 0, 0],
                             [119, 119, 119], [0.5, 1.5, 2.5],
                             [1.4, 2.4, 3.4], [0.7, 1.7, 2.7]],
                            columns=['u_p0', 'v_p0', 'w_p0'],
                            index=['k', 'x', 'y', 'z', 0, 0.9, 1.8])
    for (f, theo_df) in zip([f1, f2], [theo_df1, theo_df2]):
        sa = SonicAnemometer(loc, name, f)
        # when
        con_df = sa.measure(tb)
        # then
        pd.testing.assert_frame_equal(con_df, theo_df, check_dtype=False, check_index_type=False)


def test_cup_measure():
    """Verify cup returns correct measurement (incl. wrapping in x)"""
    # given
    loc, name = [0, 0, 119], 'test ca'
    f1, f2 = 2, 1/0.9  # (T-tstart)/dt (1) is and (2) is not an integer
    tb = TurbBox().read_uvw(**_SIMP_UVW_DICT, **_SIM_UVW_KWARGS)
    theo_df1 = pd.DataFrame([[0], [0], [0], [119], [np.sqrt(0.5**2 + 1.5**2)],
                             [np.sqrt(1**2 + 2**2)], [np.sqrt(1.5**2 + 2.5**2)],
                             [np.sqrt(1**2 + 2**2)]], columns=['u_p0'],
                            index=['k', 'x', 'y', 'z', 0, 0.5, 1, 1.5])
    theo_df2 = pd.DataFrame([[0], [0], [0], [119], [np.sqrt(0.5**2 + 1.5**2)],
                             [np.sqrt(1.4**2 + 2.4**2)], [np.sqrt(0.7**2 + 1.7**2)]],
                            columns=['u_p0'],
                            index=['k', 'x', 'y', 'z', 0, 0.9, 1.8])
    for (f, theo_df) in zip([f1, f2], [theo_df1, theo_df2]):
        ca = CupAnemometer(loc, name, f)
        # when
        con_df = ca.measure(tb)
        # then
        pd.testing.assert_frame_equal(con_df, theo_df, check_dtype=False, check_index_type=False)


if __name__ == '__main__':
    test_sonic_measure()
    test_cup_measure()
