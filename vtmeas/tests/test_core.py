# -*- coding: utf-8 -*-
"""test classes in core (MetMast and TurbBox)
"""
import pkg_resources

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest

from vtmeas import MetMast, TurbBox
from vtmeas.instruments import SonicAnemometer
from vtmeas._turb._generate_bin import _REQ_KWARGS


_SIMP_UVW_DICT = {'uvw'[ic]: np.array([[[0, 0, 0], [1, 1, 1]], [[1, 1, 1], [2, 2, 2]]])
                  + ic for ic in range(3)}  # simple uvw turbulence boxes
_SIM_UVW_KWARGS = {'ny': 2, 'nz': 3, 'dx': 1, 'dy': 180, 'dz': 90, 'u_adv': 1,
                   'z_mid': 119}  # keyword arguments for simple uvw box


@pytest.fixture
def plt_setup():
    """Pyplot setup/teardown"""
    plt.ioff()  # turn off interactive
    yield  # tests are run here
    plt.close('all')  # close all plots
    plt.ion()  # turn interactive back on


# ============================== TurbBox tests ==============================

def test_tb_read_hawc2_shape():
    """test loading a hawc2 turbulence box"""
    # given
    kwargs = {'ny': 16, 'nz': 16, 'dx': 2.3529411764705883, 'dy': 12, 'dz': 12,
              'u_adv': 10, 'z_mid': 119}
    bin_path = pkg_resources.resource_filename('vtmeas', '_turb/demo_{c}.bin')
    # when
    tb = TurbBox().read_hawc2(bin_path, **kwargs)
    # then
    np.testing.assert_array_equal(tb.u.shape[1:], (kwargs['ny'], kwargs['nz']))


def test_tb_read_uwv():
    """Verify reading uvw correctly"""
    # given & when
    tb = TurbBox().read_uvw(**_SIMP_UVW_DICT, **_SIM_UVW_KWARGS)
    # then
    np.testing.assert_array_equal(_SIMP_UVW_DICT['u'], tb.u)
    np.testing.assert_array_equal(_SIMP_UVW_DICT['v'], tb.v)
    np.testing.assert_array_equal(_SIMP_UVW_DICT['w'], tb.w)


def test_tb_interpolate():
    """Verify interpolation is correct"""
    # given
    tb = TurbBox().read_uvw(**_SIMP_UVW_DICT, **_SIM_UVW_KWARGS)
    for t_test in [0.5, 1.5]:  # test between time steps and at end
        # when
        u_int, v_int, w_int = tb.interpolate([0, 0, 119], [t_test])
        # then
        np.testing.assert_array_equal(u_int, _SIMP_UVW_DICT['u'].mean())
        np.testing.assert_array_equal(v_int, _SIMP_UVW_DICT['v'].mean())
        np.testing.assert_array_equal(w_int, _SIMP_UVW_DICT['w'].mean())


def test_tb_missing_args():
    """should raise error if args missing"""
    bad_kwargs = {'ny': 2, 'nz': 3, 'dx': 10, 'dy': 180, 'dz': 90, 'u_adv': 10}
    with pytest.raises(ValueError):
        TurbBox().read_uvw(**_SIMP_UVW_DICT, **bad_kwargs)


def test_tb_wrong_size():
    """should raise error if tb is wrong size"""
    bad_kwargs = {'ny': 1, 'nz': 1, 'dx': 1, 'dy': 1, 'dz': 1, 'u_adv': 1, 'z_mid': 119}
    with pytest.raises(ValueError):
        TurbBox().read_uvw(**_SIMP_UVW_DICT, **bad_kwargs)


def test_tb_plot_slice(plt_setup):
    """test plotting turbulence slice"""
    # given
    bin_path = bin_path = pkg_resources.resource_filename('vtmeas', '_turb/demo_{c}.bin')
    tb = TurbBox().read_hawc2(bin_path, **_REQ_KWARGS)
    # when
    fig = tb.plot_slice(tb.t[0], 'u')
    xdata, ydata = fig.axes[0].lines[0].get_data()
    assert tb.y.min() == xdata.min()
    assert tb.y.max() == xdata.max()
    assert tb.z.min() == ydata.min()
    assert tb.z.max() == ydata.max()


# ============================== MetMast tests ==============================

def test_mm_measure():
    """Two sonic anems with diff fsamp"""
    # given
    loc, name, f_samp = [0, 0, 119], 'test sa', 2
    instrs = [SonicAnemometer([0, -90, 29], 'sa p0', 1),  # should repro pt 0
              SonicAnemometer(loc, name, f_samp)]  # 2-Hz sonic in middle of box
    mm = MetMast(119, instruments=instrs)
    tb = TurbBox().read_uvw(**_SIMP_UVW_DICT, **_SIM_UVW_KWARGS)
    theo_arr_repro = np.array([ar[:, 0, 0] for ar in [tb.u, tb.v, tb.w]]).T  # instr 0
    theo_df_2hz = pd.DataFrame([[0, 1, 2], [0, 0, 0], [0, 0, 0], [119, 119, 119],
                                [0.5, 1.5, 2.5], [1, 2, 3], [1.5, 2.5, 3.5],
                                [1, 2, 3]],
                               columns=['i1_u_p0', 'i1_v_p0', 'i1_w_p0'],
                               index=['k', 'x', 'y', 'z', 0, 0.5, 1, 1.5])  # instr 1
    # when
    con_df = mm.measure(tb)
    # then
    num = con_df.index.map(lambda x: isinstance(x, (int, float)))
    np.testing.assert_array_equal(con_df.filter(regex='i0_').loc[num].dropna(),
                                  theo_arr_repro)
    pd.testing.assert_frame_equal(theo_df_2hz, con_df.filter(regex='i1_'),
                                  check_index_type=False, check_dtype=False)


def test_mm_plot(plt_setup):
    """Verify met mast plotting"""
    # given
    zs = [10, 20, 30]
    instrs = [SonicAnemometer([0, 0, z], f'sa {z}m', 1) for z in zs]
    mm = MetMast(zs[-1], instruments=instrs)
    # when
    fig = mm.plot()
    ax = fig.axes[0]
    lines = ax.get_lines()
    # then
    assert lines[0].get_data()[1][1] == zs[-1]  # rt ht
    assert sum([(lines[1 + i].get_data()[1][0] == zs[i]) for i in range(len(zs))]) == 3
    np.testing.assert_array_equal([i.name for i in instrs],
                                  ax.get_legend_handles_labels()[1])  # legend names


if __name__ == '__main__':
    test_tb_read_hawc2_shape()
    test_tb_read_uwv()
    test_tb_interpolate()
    test_tb_missing_args()
    test_tb_wrong_size()
    test_mm_measure()
