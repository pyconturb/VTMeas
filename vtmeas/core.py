# -*- coding: utf-8 -*-
"""MetMast and TurbBox classes.

These are the underlying classes for the VTMeas package. The TurbBox class is useful
for loading and visualizing the turbulence box to be measured. It also has a built-in
interpolation function that is used by the instruments when being measured. The MetMast
class collects the instruments together and interfaces with the turbulence box to
produce the final constraint dataframe. If the instruments are defined correctly, this
dataframe can be fed directly into PyConTurb for constrained turbulence generation.
"""
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.interpolate as scipint

from vtmeas._utils import bin_to_arr

_TB_REQS = ['ny', 'nz', 'dx', 'dy', 'dz', 'u_adv', 'z_mid']  # req'd for tbox location


class MetMast:
    """Virtual met mast

    Parameters
    ----------
    height : float/int
        Height of the mast.
    base : iterable, optional
        Location of the base. Default is [0, 0, 0].
    instruments : iterable, optional
        Instruments on the met mast. Only valid VTMeas-like objects are allowed (see
        documentation).
    """
    def __init__(self, height, base=[0, 0, 0], instruments=[]):
        """Initialize a MetMast object"""
        self.height = height
        self.base = base
        self.instruments = instruments

    def add_instrument(self, instrument):
        """Add instrument to the met mast

        Parameters
        ----------
        instrument : VTMeas-style instrument
            Instrument to be added to the met mast. Must have valied structure (see
            documentation).
        """
        self.instruments.append(instrument)

    def measure(self, turb_box):
        """Measure a turbulence box

        Parameters
        ----------
        turb_box : VTMeas.TurbBox
            Turbulence box to measure.

        Returns
        -------
        con_df : pandas.DataFrame
            Spatial and time information of constraint. First four rows are k, x, y, z.
            Subsequent rows correspond to time steps. Each column is a channel from an
            instrument.
        """
        con_df = pd.DataFrame()
        for i, itr in enumerate(self.instruments):
            tmp_df = itr.measure(turb_box).add_prefix(f'i{i}_')
            con_df = pd.concat((con_df, tmp_df), axis=1, sort=True)
        # sort time part of index
        num = con_df.index.map(lambda x: isinstance(x, (int, float)))
        not_num = con_df.index.map(lambda x: not isinstance(x, (int, float)))
        sort_idx = con_df.index[not_num].union(con_df.index[num].sort_values())
        return con_df.reindex(sort_idx)

    def plot(self, fig=None):
        """Plot the virtual met mast

        Parameters
        ----------
        fig : pyplot.Figure, optional
            Figure into which to plot.

        Returns
        -------
        fig : pyplot.Figure
            Figure into which the diagram was plotted.
        """
        if fig is None:
            fig = plt.figure(1, figsize=(4, 6))
        fig.clf()
        plt.plot([self.base[1], self.base[1]], [0, self.height], '0.9', lw=2)
        for instr in self.instruments:
            plt.plot(self.base[1] + instr.loc[1],
                     self.base[2] + instr.loc[2], **instr.plot_opts,
                     label=instr.name)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.tight_layout()
        return fig


class TurbBox:
    """Structured turbulence box

    Initialize using one of the ``read`` methods. Requires standard xyz grid.
    """

    def _check_add_kwargs(self, **kwargs):
        """Verify all necessary keyword arguments are given and assign them"""
        if set(_TB_REQS) - set(kwargs):
            raise ValueError('Missing keyword argument(s): ' +
                             f'{set(_TB_REQS) - set(kwargs)}')
        self.y_mid = 0  # set default lateral translation
        [setattr(self, k, v) for (k, v) in kwargs.items()]  # set passed-in args

    def read_hawc2(self, path, comps=['u', 'v', 'w'], **kwargs):
        """Load from HAWC2 binary file

        Note
        -----
        Path to binary file must have "{c}" replacing the turbulence component. For
        example, to load binary files for u and v that are called "demo_u.bin" and
        "demo_v.bin", then ``path='demo_{c}.bin``.

        Parameters
        ----------
        path : string
            Path to turbulence file with turbulent component replaced by "{c}".
        comps : iterable of str, optional
            Components to load. Default is ['u', 'v', 'w'].
        **kwargs : dict
            Keyword arguments for loading turbulence. The following arguments are
            required: ny (number points in y direction), nz (number of points in z
            direction), dx (point spacing in x direction), dy (point spacing in y
            direction), dz (point spacing in z direction), u_adv (advection speed),
            z_mid (height of center of box).

        Returns
        -------
        turb_box : VTMeas.TurbBox
            Handle to object with loaded turbulence.
        """
        self._check_add_kwargs(**kwargs)
        if '{c}' not in path:
            raise ValueError('Path must contain "{c}" for finding components!')
        turb_dict = {}
        for c in comps:  # load each requested component
            bin_path = path.replace('{c}', c)  # replace component in
            bin_arr = bin_to_arr(bin_path, self.ny, self.nz)
            turb_dict[c] = bin_arr
        self.read_uvw(**turb_dict, **kwargs)
        return self

    def read_uvw(self, u=None, v=None, w=None, **kwargs):
        """Load from numpy arrays

        Parameters
        ----------
        path : string
            Path to turbulence file with turbulent component replaced by "{c}".
        u : np.ndarray, optional
            Longitudinal component. Size is ``(nx, ny, nz)``.
        v : np.ndarray, optional
            Lateral component. Size is ``(nx, ny, nz)``.
        w : np.ndarray, optional
            Vertical component. Size is ``(nx, ny, nz)``.
        **kwargs : dict
            Keyword arguments for loading turbulence. The following arguments are
            required: ny (number points in y direction), nz (number of points in z
            direction), dx (point spacing in x direction), dy (point spacing in y
            direction), dz (point spacing in z direction), u_adv (advection speed),
            z_mid (height of center of box). Optional keyword argument is y_mid, the
            lateral location of the box center.

        Returns
        -------
        turb_box : VTMeas.TurbBox
            Handle to object with loaded turbulence.
        """
        self._check_add_kwargs(**kwargs)
        self.comps = []
        for (s, arr) in [('u', u), ('v', v), ('w', w)]:
            if arr is not None:
                if not np.array_equal(arr.shape[1:], (self.ny, self.nz)):
                    raise ValueError('TurbBox size doesn\'t match given ny, nz!')
                setattr(self, s, arr)
                self.comps.append(s)
                self.nx = arr.shape[0]
        self.shape = self.nx, self.ny, self.nz
        ny, nz, dy, dz = self.ny, self.nz, self.dy, self.dz
        self.y = np.linspace(-dy*(ny - 1)/2, dy*(ny - 1)/2, ny) + self.y_mid
        self.z = np.linspace(-dz*(nz - 1)/2, dz*(nz - 1)/2, nz) + self.z_mid
        self.t = np.arange(self.nx) * self.dx/self.u_adv
        self.T = self.t[-1] + self.dx/self.u_adv
        return self

    def interpolate(self, locs, t_new=None, comps=None):
        """Interpolate turbulence box to new locations and time steps

        Note
        -----
        The interpolation is first within each 2D time slice using 2D linear
        interpolation. It is then interpolated to the new time stamps using 1D linear
        interpolation. The turbulence box is assumed to wrap after T seconds.

        Parameters
        ----------
        locs : np.ndarray (or similar)
            Locations of points for interpolation. Size is ``(npt, nd)``.
        t_new : list/np.ndarray, optional
            New time steps to be interpolated to. Default is the time steps in the
            turbulence box.
        comps : iterable, optional
            Turbulence components to interpolate. Default is all components in box.

        Returns
        -------
        int_arr : np.ndarray
            Array of interpolated points/times. Size is ``(nc, ntnew, npnew)``.
        """
        if comps is None:  # comps by default is what's loaded
            comps = self.comps
        if t_new is None:  # t_new by default is same as turbulence box
            t_new = self.t
        locs = np.atleast_2d(locs)
        nc, ntnew, npt = len(comps), len(t_new), locs.shape[0]
        int_arr = np.empty((nc, ntnew, npt))
        # STEP 1: interpolate in 2D plane to requested locations
        ymeas, zmeas = locs[:, 1], locs[:, 2]
        ym_unique, zm_unique = np.unique(ymeas), np.unique(zmeas)
        ym_mesh, zm_mesh = np.meshgrid(ym_unique, zm_unique)  # grid formed by ms pts
        for ic, c in enumerate(comps):
            turb_arr = getattr(self, c)
            # perform 2d linear plane interpolation
            int_2d_arr = np.empty((nc, self.shape[0], npt))
            for it, turb_slice in enumerate(turb_arr):
                interp_func = scipint.interp2d(self.y, self.z, turb_slice.T)
                int_slice = np.atleast_2d(interp_func(ym_unique, zm_unique))
                # pull out requested points using magic (sorry...)
                i_idx = (zm_mesh[:, 0, None] == zmeas[:, None].T).argmax(0)  # row indcs
                j_idx = (ym_mesh == ymeas[:, None]).argmax(1)  # column indices
                int_2d_arr[ic, it, :] = int_slice[i_idx, j_idx]  # just req'd points
            # STEP 1.5: add ending time to turbulence box (repeating)
            t_ext = np.concatenate((self.t, [self.T]))
            int_t_arr = np.r_[int_2d_arr[ic, :, :], int_2d_arr[ic, 0, :].reshape(1, -1)]
            # SECOND. Interpolate in 1D to requested times.
            for i in range(int_t_arr.shape[1]):
                try:
                    interp_func = scipint.interp1d(t_ext, int_t_arr[:, i], kind='cubic')
                except ValueError:  # if only 2 time points, do linear
                    interp_func = scipint.interp1d(t_ext, int_t_arr[:, i], kind='linear')
                int_arr[ic, :, i] = interp_func(t_new)
        return int_arr

    def plot_slice(self, t, comp, num=None):
        """Plot a slice of the turbulence

        Parameters
        ----------
        t : int/float
            Time to plot. Must be a time existing within the TurbBox object.
        comp : str
            Component to plot ('u', 'v' or 'w').
        num : int, optional
            Figure number into which to plot. Default is to create a new figure.

        Returns
        -------
        fig : pyplot.Figure
            Handle to figure.
        """
        if t not in self.t:
            raise ValueError('Requested slice not in turbulence box!')
        idx = (t == self.t).argmax()
        arr = getattr(self, comp)
        turb_slice = arr[idx]
        fig = plt.figure(num=num, figsize=(6.2, 5), clear=True)
        plt.imshow(turb_slice.T, extent=[self.y[0]-self.dy/2, self.y[-1]+self.dy/2,
                                         self.z[0]-self.dz/2, self.z[-1]+self.dz/2],
                   origin='lower')
        plt.colorbar()
        ymesh, zmesh = np.meshgrid(self.y, self.z)
        plt.plot(ymesh.flatten(), zmesh.flatten(), 'wo', mec='0.4', alpha=0.4, ms=4)
        plt.title(f'{comp} slice: t = {t:.2f}')
        return fig
