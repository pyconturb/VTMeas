# -*- coding: utf-8 -*-
"""Utilities
"""
import numpy as np


_HAWC2_BIN_FMT = '<f'  # HAWC2 binary turbulence datatype


def bin_to_arr(path, ny, nz):
    """Load binary file to array"""
    bin_arr = np.fromfile(path, dtype=np.dtype(_HAWC2_BIN_FMT))
    nx = bin_arr.size // (ny * nz)
    if (nx * ny * nz) != bin_arr.size:
        raise ValueError('Binary file size does not match input sizes!')
    bin_arr.shape = (nx, ny, nz)
    return bin_arr
