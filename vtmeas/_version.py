# this should be updated when git is tagged
#   version:    0.0     First creation
#   version:    0.1     First working version with docs
#   version:    0.2     Changed con_df to match PCT, add cup
__version__ = '0.2.dev0'
__release__ = '0.2.dev0'
