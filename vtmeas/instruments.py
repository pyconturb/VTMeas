# -*- coding: utf-8 -*-
"""Instrument classes for measuring turbulence.

These classes define the measuring logic for each instrument. The instruments are then
collected on a MetMast object for the ultimate sampling of a turbulence box.

Custom instruments can be created as long as they have the correct ``measure`` method
(see Examples).
"""
import numpy as np
import pandas as pd


class SonicAnemometer:
    """Standard 3D sonic anemometer

    Parameters
    ----------
    loc : iterable
        Location of the instrument.
    name : string
        Name for the instrument.
    f_samp : int/float
        Sampling frequency in Hz.
    t0 : int, optional
        Starting time for sampling (optional).
    """
    def __init__(self, loc, name, f_samp, t0=0):
        """loc is 3-element 1d array of location. assumes sampling starts at 0."""
        self.loc = loc
        self.name = name
        self.f_samp = f_samp
        self.t0 = t0
        self.plot_opts = {'marker': 'X', 'color': 'r', 'ms': 12}

    def measure(self, turb_box):
        """Measure the turbulence box and return a pandas dataframe

        Parameters
        ----------
        turb_box : VTMeas.TurbBox
            Turbulence box to measure.

        Returns
        -------
        con_df : pandas.DataFrame
            Spatial and time information of constraint. First four rows are k, x, y, z.
            Subsequent rows correspond to time steps. Each column is a channel from the
            instrument.
        """
        tb_t, dt = turb_box.t, 1/self.f_samp
        t_samp = np.arange(np.ceil((tb_t[-1] + tb_t[1] - 2*tb_t[0])/dt))*dt + self.t0
        turb_int = turb_box.interpolate(self.loc, t_new=t_samp, comps='uvw')
        index = pd.concat((pd.Series(['k', 'x', 'y', 'z']), pd.Series(t_samp)))
        con_df = pd.DataFrame(index=index)
        for ic, arr in enumerate(turb_int):
            new_df = pd.DataFrame(index=index, columns=[f'{"uvw"[ic]}_p{i}' for i in
                                                        range(arr.shape[1])])
            new_df.loc['k'] = ic
            new_df.loc['x'] = self.loc[0]
            new_df.loc['y'] = self.loc[1]
            new_df.loc['z'] = self.loc[2]
            new_df.loc[t_samp] = arr
            con_df = pd.concat((con_df, new_df), axis=1)
        return con_df


class CupAnemometer:
    """Standard cup anemometer

    Parameters
    ----------
    loc : iterable
        Location of the instrument.
    name : string
        Name for the instrument.
    f_samp : int/float
        Sampling frequency in Hz.
    t0 : int, optional
        Starting time for sampling (optional).
    """
    def __init__(self, loc, name, f_samp, t0=0):
        """loc is 3-element 1d array of location. assumes sampling starts at 0."""
        self.loc = loc
        self.name = name
        self.f_samp = f_samp
        self.t0 = t0
        self.plot_opts = {'marker': 'o', 'color': 'b', 'ms': 12}

    def measure(self, turb_box):
        """Measure the turbulence box and return a pandas dataframe

        Parameters
        ----------
        turb_box : VTMeas.TurbBox
            Turbulence box to measure.

        Returns
        -------
        con_df : pandas.DataFrame
            Spatial and time information of constraint. First four rows are k, x, y, z.
            Subsequent rows correspond to time steps. Each column is a channel from the
            instrument.
        """
        tb_t, dt = turb_box.t, 1/self.f_samp
        t_samp = np.arange(np.ceil((tb_t[-1] + tb_t[1] - 2*tb_t[0])/dt))*dt + self.t0
        u_int, v_int = turb_box.interpolate(self.loc, t_new=t_samp, comps='uv')
        index = pd.concat((pd.Series(['k', 'x', 'y', 'z']), pd.Series(t_samp)))
        con_df = pd.DataFrame(index=index)
        con_df.loc['k', 'u_p0'] = 0  # constrain u direction
        con_df.loc[['x', 'y', 'z'], 'u_p0'] = self.loc
        con_df.loc[t_samp, 'u_p0'] = np.sqrt(u_int**2 + v_int**2).squeeze()
        return con_df
