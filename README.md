[![pipeline status](https://gitlab.windenergy.dtu.dk/rink/pyconturb/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/rink/VTMeas/commits/master)
[![coverage report](https://gitlab.windenergy.dtu.dk/rink/pyconturb/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/rink/VTMeas/commits/master)

# Virtual Measurements

Virtual measurements of turbulence boxes for generating constrained turbulence

## Installation and Examples

See the [documentation website](https://rink.pages.windenergy.dtu.dk/VTMeas/).